#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Sum columns in a .csv file')
parser.add_argument('-i', '--input', help='.csv file to sum') 
parser.add_argument('-o', '--output', help='Result')
parser.add_argument('-j', '--json', help='Column summation mapping')
parser.add_argument('-c', '--kept-columns', nargs='+', help='Kept columns')
args = parser.parse_args()

import json
import pandas as pd
import numpy as np


df = pd.read_csv(args.input)
with open(args.json) as mapping_file:
    mapper = json.load(mapping_file)

new_columns = [df[args.kept_columns]]
for key, value in mapper.items():
    source_columns = df[value]
    target_column = source_columns.sum(axis=1)
    target_column = target_column.rename(key).to_frame()
    new_columns.append(target_column)
new_df = pd.concat(new_columns, axis=1)
new_df.to_csv(args.output, index=False)
