#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Split rows of a .csv file')
parser.add_argument('-i', '--input', required=True, help='.csv file to split') 
parser.add_argument('-o', '--output-dir', required=True, help='Result folder')
parser.add_argument('-c', '--ref-column', required=True,
                    help='Split the dataframe using the values from the column')
parser.add_argument('-d', '--delimiter', default=',', help='.csv delimiter')
args = parser.parse_args()

import pandas as pd
import numpy as np
import os


if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

df = pd.read_csv(args.input, sep=args.delimiter)
for name, group in df.groupby(args.ref_column):
    filename, ext = os.path.splitext(os.path.basename(args.input))
    suffix = '-'.join([args.ref_column, str(name)])
    output_filename = '_'.join([filename, suffix]) + ext
    output_filename = os.path.join(args.output_dir, output_filename)
    # group['ref_column'] = name
    group.to_csv(output_filename, index=False)
